export class Pageable {
  public offset: number;
  public pageNumber: number;
  public pageSize: number;
  public paged: boolean;
  public unpaged: boolean;
}
