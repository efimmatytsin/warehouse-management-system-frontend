export class TableStateChange {
  public page: number;
  public pageSize: number;
  public search: string;
}
