export class Product {
  public brand: string;
  public groupId: string;
  public id: string;
  public price: number;
  public quantity: number;
  public size: number;
  public title: string;

}
