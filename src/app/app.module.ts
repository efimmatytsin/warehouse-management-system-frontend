import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppComponent} from './app.component';
import {FormsModule} from "@angular/forms";
import {HTTP_INTERCEPTORS, HttpClientModule} from "@angular/common/http";
import {RouterModule, Routes} from "@angular/router";
import {CookieService} from "ngx-cookie-service";
import {AuthenticationInterceptor} from "./interceptor/auth.interceptor";
import {LoginComponent} from "./component/login/login.component";
import {AuthGuard} from "./guard/auth.guard";
import {OAuthService} from "./service/oauth.service";
import {DashboardComponent} from "./component/dashboard/dashboard.component";
import {ProductService} from "./service/product.service";
import {ProductsComponent} from "./component/products/products.component";
import {ProductsTableComponent} from "./component/products/table/products.table.component";
import {ProductComponent} from "./component/product/product.component";
import {NotifyService} from "./service/notify.service";
import {WillEndSoonProductsComponent} from "./component/products/will-end-soon/will.end.soon.products.component";
import {ProductsImportComponent} from "./component/products/import/products.import.component";

const routes: Routes = [
  {
    path: 'dashboard',
    canActivate: [AuthGuard],
    component: DashboardComponent,
    children:[
      {
        path: 'products',
        component: ProductsComponent
      },
      {
        path: 'product/:id',
        component: ProductComponent
      },
      {
        path: 'product/create',
        component: ProductComponent
      },
      {
        path: 'products/will-end-soon',
        component: WillEndSoonProductsComponent
      }
    ]
  },{
    path: '',
    pathMatch: 'full',
    component: LoginComponent,
    canActivate: [AuthGuard]
  }, {
    path: 'login',
    component: LoginComponent
  }
];

@NgModule({
  declarations: [
    AppComponent,
    DashboardComponent,
    ProductsTableComponent,
    ProductComponent,
    LoginComponent,
    ProductsImportComponent,
    WillEndSoonProductsComponent,
    ProductsComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    RouterModule.forRoot(routes),
    HttpClientModule,
  ],
  providers: [
    OAuthService,
    CookieService,
    NotifyService,
    ProductService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthenticationInterceptor,
      multi: true,
    }],
  bootstrap: [AppComponent]
})
export class AppModule {
}
