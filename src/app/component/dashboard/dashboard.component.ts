import {Component} from "@angular/core";
import {OAuthService} from "../../service/oauth.service";
import {Router} from "@angular/router";

@Component({
  templateUrl: 'dashboard.component.html'
})
export class DashboardComponent {

  constructor(private oauthService: OAuthService, private router: Router){}

  onLogoutButtonClick(){
    this.oauthService.logout().subscribe((data:any) => {
      this.router.navigate(["/login"]);
    })
  }
}
