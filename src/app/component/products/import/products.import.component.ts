import {Component, EventEmitter, Input, Output} from "@angular/core";
import {ProductService} from "../../../service/product.service";
import {NotifyService} from "../../../service/notify.service";
import {HttpErrorResponse} from "@angular/common/http";

@Component({
  selector: 'products-import',
  templateUrl: 'products.import.component.html'
})
export class ProductsImportComponent {

  private file: File;

  @Input()
  public type: string;

  @Output()
  public onImport: EventEmitter<any> = new EventEmitter();

  constructor(private productService: ProductService, private notifyService: NotifyService) {
  }

  selectFile(files: File[]) {
    for (let i = 0; i < files.length; i++) {
      this.file = files[i];
    }

  }

  onSubmit() {
    if (this.file != null) {
      if(this.type == 'xls') {
        this.productService.importXls(this.file).subscribe((data: any) => {
          this.onImport.emit(true);
          this.notifyService.successNotify("Imported successful!!");
        }, (response: HttpErrorResponse) => {
          this.notifyService.errorNotify(response.error.message);
        })
      }
      if(this.type == 'csv') {
        this.productService.importCsv(this.file).subscribe((data: any) => {
          this.onImport.emit(true);
          this.notifyService.successNotify("Imported successful!!");
        }, (response: HttpErrorResponse) => {
          this.notifyService.errorNotify(response.error.message);
        })
      }
    }
  }

}
