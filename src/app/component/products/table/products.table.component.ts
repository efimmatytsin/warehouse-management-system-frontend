import {Component, EventEmitter, Input, OnInit, Output} from "@angular/core";
import {Page} from "../../../model/page";
import {Product} from "../../../model/product";
import {Pageable} from "../../../model/pageable";
import {TableStateChange} from "../../../model/table.state.change";
import {ProductService} from "../../../service/product.service";
import {Router} from "@angular/router";
import {NotifyService} from "../../../service/notify.service";
import {OAuthService} from "../../../service/oauth.service";

@Component({
  templateUrl: 'products.table.component.html',
  selector: 'products-table'
})
export class ProductsTableComponent implements OnInit {

  pageSize: number = 5;

  currentPage: number = 1;

  @Input()
  public showSearch: boolean = true;

  @Input()
  public productsPage: Page<Product>;

  public search: string = '';

  @Output()
  public refresh: EventEmitter<TableStateChange> = new EventEmitter();

  constructor(private productService: ProductService, private router: Router,
              public oauthService: OAuthService,
              private notifyService: NotifyService) {
  }

  ngOnInit(): void {
    this.refreshTable();
  }

  private onPageSelect(pageSelected: number) {
    this.currentPage = pageSelected;
    this.refreshTable();
  }

  private refreshTable() {
    let tableStateChange = new TableStateChange();
    tableStateChange.page = this.currentPage-1;
    tableStateChange.pageSize = this.pageSize;
    tableStateChange.search = this.search;
    this.refresh.emit(tableStateChange);
  }

  onSearchChange(data: string) {
    this.search = data;
    this.refreshTable();
  }

  onDeleteButtonClick(id: string) {
    this.productService.delete(id).subscribe((data: any) => {
      this.refreshTable();
      this.notifyService.successNotify("Deleted!");
    })
  }

  onEditButtonClick(id: string) {
    this.router.navigate(['/dashboard/product/'+id]);
  }

  onPageSizeChange(size: number) {
    this.pageSize = size;
    this.currentPage = 1;
    this.refreshTable();
  }

}
