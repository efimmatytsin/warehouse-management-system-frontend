import {Component, OnInit} from "@angular/core";
import {ProductService} from "../../service/product.service";
import {Page} from "../../model/page";
import {Product} from "../../model/product";
import {HttpErrorResponse, HttpEvent, HttpResponse} from "@angular/common/http";
import {TableStateChange} from "../../model/table.state.change";
import {Router} from "@angular/router";
import * as FileSaver from 'file-saver';
import {ProductExportRequest} from "../../model/product.export.request";
import {NotifyService} from "../../service/notify.service";
import {OAuthService} from "../../service/oauth.service";

@Component({
  templateUrl: 'products.component.html'
})
export class ProductsComponent implements OnInit {

  public productsData: Page<Product>;

  private lastStateChange: TableStateChange;

  constructor(private productService: ProductService, private router: Router,
              public oauthService: OAuthService,
              private notifyService: NotifyService) {
  }

  ngOnInit(): void {

  }

  onTableRefresh(data: TableStateChange) {
    this.lastStateChange = data;
    this.productService.filtered(data.search, data.page, data.pageSize).subscribe((data: Page<Product>) => {
      this.productsData = data;
    }, (errorResponse: HttpErrorResponse) => {
      this.notifyService.errorNotify(errorResponse.error.error_description);
    });
  }

  onCreateButtonClick(): void {
    this.router.navigate(['/dashboard/product/create']);
  }

  onImportComplete(): void {
    this.onTableRefresh(this.lastStateChange);
  }

  onExportXlsButtonClick(): void {
    let data = new ProductExportRequest();
    data.products = [];
    if (this.productsData != null) {
      let products = this.productsData.content;
      for (let i = 0; i < products.length; i++) {
        data.products.push(products[i].id)
      }
      this.productService.exportXls(data).subscribe((response: any) => {
        var now = new Date();
        FileSaver.saveAs(response, now.toDateString() + ".xls");
      });
    }

  }

  onExportCsvButtonClick(): void {
    let data = new ProductExportRequest();
    data.products = [];
    if (this.productsData != null) {
      let products = this.productsData.content;
      for (let i = 0; i < products.length; i++) {
        data.products.push(products[i].id)
      }
      this.productService.exportCSV(data).subscribe((response: any) => {
        var now = new Date();
        FileSaver.saveAs(response, now.toDateString() + ".csv");
      });
    }

  }

}
