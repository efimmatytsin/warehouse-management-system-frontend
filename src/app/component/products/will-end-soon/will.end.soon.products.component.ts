import {Component} from "@angular/core";
import {Page} from "../../../model/page";
import {Product} from "../../../model/product";
import {ProductService} from "../../../service/product.service";
import {Router} from "@angular/router";
import {TableStateChange} from "../../../model/table.state.change";
import {HttpErrorResponse} from "@angular/common/http";

@Component({
  templateUrl: 'will.end.soon.products.component.html'
})
export class WillEndSoonProductsComponent {
  public productsData: Page<Product>;

  constructor(private productService: ProductService, private router: Router) {
  }

  onTableRefresh(data: TableStateChange) {
    this.productService.findByQuantityLessThanEqual(5, data.page, data.pageSize).subscribe((data: Page<Product>) => {
      this.productsData = data;
    }, (errorResponse: HttpErrorResponse) => {
      alert(JSON.stringify(errorResponse.error))
    });
  }
}
