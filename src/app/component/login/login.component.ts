import {AfterViewInit, Component} from "@angular/core";
import {LoginFormModel} from "../../model/login.form.model";
import {OAuthService} from "../../service/oauth.service";
import {HttpErrorResponse, HttpResponse} from "@angular/common/http";
import {ErrorResponse} from "../../model/error.response";
import {Router} from "@angular/router";
import {NotifyService} from "../../service/notify.service";

@Component({
  templateUrl: 'login.component.html',
  styleUrls: ['login.component.css']
})
export class LoginComponent implements AfterViewInit {

  loginFormModel: LoginFormModel = new LoginFormModel();

  constructor(private oauthService: OAuthService, private router: Router,
              private notifyService: NotifyService) {
  }

  ngAfterViewInit(): void {
    this.oauthService.clearAuthData();
    this.loginFormModel = new LoginFormModel();
  }


  onSubmit(): void {
    console.dir(this.loginFormModel);
    this.oauthService.login(this.loginFormModel).subscribe((data: any) => {
      this.oauthService.storeAuth(data, true);
      this.router.navigate(["/dashboard/products"]);
    }, (response: HttpErrorResponse) => {
      this.notifyService.errorNotify(response.error.error_description);
    });
  }

}
