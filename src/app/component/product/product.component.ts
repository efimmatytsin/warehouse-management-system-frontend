import {Component, OnInit} from "@angular/core";
import {Product} from "../../model/product";
import {ActivatedRoute, Params, UrlSegment} from "@angular/router";
import {ProductService} from "../../service/product.service";
import {HttpErrorResponse} from "@angular/common/http";
import {NotifyService} from "../../service/notify.service";

@Component({
  templateUrl: 'product.component.html'
})
export class ProductComponent implements OnInit {

  product: Product;

  constructor(private route: ActivatedRoute, private productService: ProductService, private notifyService: NotifyService) {
  }

  ngOnInit(): void {
    this.route.url.subscribe((data: UrlSegment[]) => {
      if (data[data.length - 1].path == 'create') {
        this.product = new Product();
      }
    });
    this.route.params.subscribe((data: Params) => {
      const id = data['id'];
      this.productService.findById(id).subscribe((p: Product) => {
        this.product = p;
      })
    });
  }

  onSubmit() {
    this.productService.save(this.product).subscribe((p: Product) => {
        this.product = p;
        this.notifyService.successNotify("Product saved!");
      },
      (response: HttpErrorResponse) => {
        this.notifyService.errorNotify(response.error.error_description);
      });
  }
}
