import {HttpClient, HttpHeaders} from "@angular/common/http";
import {Injectable} from "@angular/core";
import {CookieService} from "ngx-cookie-service";
import {Observable} from "rxjs";
import {AuthData} from "../model/auth.data";
import {LoginFormModel} from "../model/login.form.model";
import {AppModule} from "../app.module";
import {AppParams} from "../app.params";

@Injectable()
export class OAuthService {

  public static AUTH_KEY = 'auth_back';

  private static AUTH_TIME = 'auth_time_back';

  private static REFRESH_TIME = 'refresh_time_back';

  private authData: AuthData;

  constructor(private http: HttpClient, private cookieService: CookieService) {
    let text = this.cookieService.get(OAuthService.AUTH_KEY);
    if(text != '') {
      this.authData = JSON.parse(text);
    }
  }

  public storeAuth(authData: AuthData, remeber: boolean) {
    this.cookieService.set(OAuthService.AUTH_KEY, JSON.stringify(authData));
    if (remeber) {
      this.cookieService.set(OAuthService.AUTH_TIME, ((new Date()).getTime() + 3600 * 24 * 365)+"");
    } else {
      this.cookieService.set(OAuthService.AUTH_TIME, (new Date()).getTime()+"");
    }

    this.cookieService.set(OAuthService.REFRESH_TIME, (new Date()).getTime()+"");

    this.authData = authData;
  }

  public isAdmin(){
    return this.authData.role == 'ADMIN';
  }

  public logout() {
    let url = AppParams.BE_API_URL + 'oauth/logout';
    let authData = this.getAuthData();
    if (authData !== undefined && authData != null) {
      let headers = new HttpHeaders({'Authorization': authData.token_type + ' ' + authData.access_token});
      let options ={headers: headers};

      return this.http.get(url, options);
    } else {
      let headers = new HttpHeaders({'Authorization': AppParams.BE_AUTH_TOKEN});
      let options ={headers: headers};

      return this.http.get(url, options);
    }
  }

  public getAuthTime(): number {
    return +this.cookieService.get(OAuthService.AUTH_TIME);
  }

  public getRefreshTime(): number {
    return +this.cookieService.get(OAuthService.REFRESH_TIME);
  }

  public setRefreshTime() {
    this.cookieService.set(OAuthService.REFRESH_TIME, (new Date()).getTime()+"");
  }

  public getAuthData(): AuthData {
    return this.authData;
  }

  public login(loginModel: LoginFormModel): Observable<any> {
    let url = AppParams.BE_API_URL + "oauth/token";
    let model: FormData = new FormData();
    model.append('username', loginModel.username);
    model.append('password', loginModel.password);
    model.append('grant_type', 'password');

    let headers = new HttpHeaders({'Authorization': AppParams.BE_AUTH_TOKEN});
    let options = {headers: headers};
    return this.http.post(url, model, options);
  }

  public refreshToken(token: string): Observable<any> {
    var url = AppParams.BE_API_URL + "oauth/token?grant_type=refresh_token&refresh_token=" + token;

    let headers = new HttpHeaders({'Authorization': AppParams.BE_AUTH_TOKEN});

    let options = {headers: headers};

    return this.http.post(url, {}, options);
  }

  public clearAuthData() {
    this.authData = null;
    this.cookieService.delete(OAuthService.AUTH_KEY);
    this.cookieService.delete(OAuthService.AUTH_TIME);
    this.cookieService.delete(OAuthService.REFRESH_TIME);
  }

}
