import {Injectable} from "@angular/core";
import {HttpClient, HttpHeaders, HttpResponse} from "@angular/common/http";
import {AppParams} from "../app.params";
import {Observable} from "rxjs";
import {Page} from "../model/page";
import {Product} from "../model/product";
import {ProductExportRequest} from "../model/product.export.request";

@Injectable()
export class ProductService {
  constructor(private http: HttpClient){}

  public filtered(search: string, page: number, size: number): Observable<Page<Product>> {
    let url = AppParams.BE_API_URL + 'product/filtered?search='+ search+'&page='+page+'&size='+size;
      return this.http.get(url) as Observable<Page<Product>>;
  }

  public delete(id: string): Observable<any> {
    let url = AppParams.BE_API_URL + 'product/'+ id;
    return this.http.delete(url) as Observable<any>;
  }

  public findById(id: string): Observable<Product> {
    let url = AppParams.BE_API_URL + 'product/'+ id;
    return this.http.get(url) as Observable<Product>;
  }

  public save(product: Product): Observable<Product> {
    let url = AppParams.BE_API_URL + 'product/';
    return this.http.post(url, product) as Observable<Product>;

  }

  public exportXls(data: ProductExportRequest): Observable<Blob>{
    let url = AppParams.BE_API_URL + 'product/export/xls';
    return this.http.post(url, data,{
      responseType: "blob"
    }) as Observable<Blob>;

  }

  public exportCSV(data: ProductExportRequest): Observable<Blob>{
    let url = AppParams.BE_API_URL + 'product/export/csv';
    return this.http.post(url, data,{
      responseType: "blob"
    }) as Observable<Blob>;

  }

  public findByQuantityLessThanEqual(quantity: number, page: number, size: number): Observable<Page<Product>> {
    let url = AppParams.BE_API_URL + 'product/filtered/quantity?quantity='+ quantity+'&page='+page+'&size='+size;
    return this.http.get(url) as Observable<Page<Product>>;
  }

  public importXls(file: File): Observable<any> {
    const formData = new FormData();
    formData.append('file', file);
    let url = AppParams.BE_API_URL + 'product/import/xls';
    return this.http.post(url, formData) as Observable<any>;
  }

  public importCsv(file: File): Observable<any> {
    const formData = new FormData();
    formData.append('file', file);
    let url = AppParams.BE_API_URL + 'product/import/csv';
    return this.http.post(url, formData) as Observable<any>;
  }
}
