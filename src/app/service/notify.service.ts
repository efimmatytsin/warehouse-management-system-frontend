import {Injectable} from "@angular/core";

declare var $: any;

@Injectable()
export class NotifyService {

  public successNotify(message: string): void{
    $.notify({
      icon: "tim-icons icon-bell-55",
      message: message

    },{
      type: 'success',
      timer: 1000,
      placement: {
        from: 'top',
        align: 'right'
      }
    });
  }

  public errorNotify(message: string): void{
    $.notify({
      icon: "tim-icons icon-bell-55",
      message: message

    },{
      type: 'danger',
      timer: 1000,
      placement: {
        from: 'top',
        align: 'right'
      }
    });
  }
}
