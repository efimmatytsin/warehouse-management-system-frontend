export class AppParams {
  public static BE_API_URL = 'http://localhost:8080/api/';
  public static BE_AUTH_TOKEN = 'Basic ZGVmYXVsdDoxMTIyMzM=';
  public static AUTH_KEY_COOKIE = 'auth_back';
}
