import {
  HttpErrorResponse,
  HttpEvent,
  HttpHandler,
  HttpInterceptor,
  HttpRequest,
  HttpResponse
} from "@angular/common/http";
import {Observable, throwError} from "rxjs";
import {catchError, map, tap} from 'rxjs/operators';
import {Injectable} from "@angular/core";
import {CookieService} from "ngx-cookie-service";
import {Router} from "@angular/router";
import {OAuthService} from "../service/oauth.service";

@Injectable()
export class AuthenticationInterceptor implements HttpInterceptor {

  constructor(private cookieService: CookieService,
              private oauthService: OAuthService,
              private router: Router){}

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    let text = this.cookieService.get('auth_back');
    if(text != '') {
      var authData = JSON.parse(text);
      if (authData != null) {
        req = req.clone({
          setHeaders: {
            Authorization: authData.token_type + ' ' + authData.access_token
          }
        });
      }
    }
    return next.handle(req).pipe(
      tap((event: HttpEvent<any>) => {
        return event;
      }),
      catchError(response => {
        if (response instanceof HttpErrorResponse) {
          if(response.status == 401){
            this.oauthService.clearAuthData();
            this.router.navigate(["/login"]);
          }
        }

        return throwError(response);
      })
    );
  }
}
