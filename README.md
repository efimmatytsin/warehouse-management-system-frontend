# WAREHOUSE MANAGEMENT SYSTEM

Login screen:
![alt text](./images/1.png)
After successful login, you will be redirect to  products page:
![alt text](./images/2.png)
On bottom page, you can import products in CSV/XLS
![alt text](./images/3.png)
You can create new on product or modify exist one:
![alt text](./images/4.png)
If you open will end soon page, you will see products with quantity less than five:
![alt text](./images/5.png)
System have roles functionality, if you login without admin authority, you cant change/delete/create products:
![alt text](./images/6.png)


## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.
